import mysql from 'mysql';  //import mysql package
import express from 'express'; //import Express package
import bodyparser from 'body-parser'; //import body-parser package

var app = express();

app.use(bodyparser.json());

//create mysql connection
var mysqlConnection = mysql.createConnection({
    // host: 'localhost',
    // user: 'root',
    // password: '26012016',
    // database: 'EmployeeDB',
    // multipleStatements: true

    host: 'database-1.cl0djy4eofkq.us-west-1.rds.amazonaws.com',
    port: '3306',
    user: 'admin',
    password: '26012016',
    database: 'EmployeeDB',
});

mysqlConnection.connect((err) => {
    if (!err)
        console.log('DB connection succeded.');
    else
        console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});

//start the server
app.listen(5000, () => console.log('Express server is runnig at port no : 5000'));


//--------------Get all employees------------------------------
app.get('/employees', (req, res) => {
    mysqlConnection.query('SELECT * FROM employee;', (err, rows, fields) => {
        if (!err) {
            res.status(200);
            res.send(rows);
        }
        else
            console.log(err);
    })
});

//----------------------create employee-----------------------
app.post('/employees', (req, res) => {
    const employee = req.body;
    var sql = 'INSERT INTO employee SET ?';

    try {
        mysqlConnection.query(sql, employee, function (err, data, fields) {
            if (err) throw err;
            res.send("Employee ID : " + data.insertId)
            res.status(201)
        });
    } catch (error) {
        console.log(error)
    }

});


//----------------------update employee----------------------
app.put('/employees/:id', (req, res) => {
    const employeeID = req.params.id;
    const employee = req.body;
    var sql = 'UPDATE employee SET ? where EmpID = ?';

    try {
        mysqlConnection.query(sql, [employee, employeeID], function (err, data, fields) {
            if (err) {
                console.log(err)
            }
            res.send(data)
            res.status(200)
        });
    } catch (error) {
        console.log(error)
    }

});


//----------------------Get an employees----------------------
app.get('/employees/:id', (req, res) => {
    mysqlConnection.query('SELECT * FROM employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Delete an employees
app.delete('/employees/:id', (req, res) => {
    mysqlConnection.query('DELETE FROM employee WHERE EmpID = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Deleted successfully.');
        else
            console.log(err);
    })
});
